'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Location Schema
 */
var LocationSchema = new Schema({
    name: String,
    locationType: String,
    loc: {
        lat: Number,
        lon: Number
    },
    rating: {
        type: Number,
        min: 0,
        max: 5
    }
});

mongoose.model('Location', LocationSchema);
