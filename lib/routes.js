'use strict';

var api = require('./controllers/api'),
    index = require('./controllers');

/**
 * Application routes
 */
module.exports = function (app) {

    app.get('/locations', api.allLocations);

    app.get('/bar', function(req, res) {
        req.query.type = 'bar';
        return api.allLocationsOfType(req, res)
    });

    app.get('/restaurant', function(req, res) {
        req.query.type = 'restaurant';
        return api.allLocationsOfType(req, res)
    });

    app.get('/shop', function(req, res) {
        req.query.type = 'shop';
        return api.allLocationsOfType(req, res)
    });

    app.get('/minrating', api.minRatingLocations);


    app.get('/*', function (req, res) {
        res.send(404);
    });
};