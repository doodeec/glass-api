'use strict';

var mongoose = require('mongoose'),
    Location = mongoose.model('Location');

/**
 * Populate database with sample application data
 */

//Clear old things, then add things in
Location.find({}).remove(function () {
    Location.create({
            name: 'Moj bar',
            locationType: 'bar',
            loc: {
                lat: 41.5,
                lon: 53.1
            },
            rating: 4.1
        }, {
            name: 'Tvoj bar',
            locationType: 'bar',
            loc: {
                lat: 41.7,
                lon: 53.2
            },
            rating: 3.7
        }, {
            name: 'Moja restika',
            locationType: 'restaurant',
            loc: {
                lat: 41.9,
                lon: 53.0
            },
            rating: 4.3
        }, /* real data from this point */ {
            name: 'Klub Fléda',
            locationType: 'bar',
            loc: {
                lat: 49.209906,
                lon: 16.603368
            },
            rating: 4.1
        }, {
            name: 'Comodo bar restaurant',
            locationType: 'restaurant',
            loc: {
                lat: 49.210195,
                lon: 16.601949
            },
            rating: 3.8
        }, {
            name: 'Sklípek',
            locationType: 'restaurant',
            loc: {
                lat: 49.208926,
                lon: 16.600405
            },
            rating: 2.8
        }, {
            name: 'Restaurace Sport bar Gambrinus',
            locationType: 'restaurant',
            loc: {
                lat: 49.215872,
                lon: 16.599729
            },
            rating: 0
        }, {
            name: 'SEVERKA - RESTAURACE, VINÁRNA',
            locationType: 'restaurant',
            loc: {
                lat: 49.207756,
                lon: 16.597765
            },
            rating: 3.3
        }, {
            name: 'Klub Alterna',
            locationType: 'bar',
            loc: {
                lat: 49.210447,
                lon: 16.594718
            },
            rating: 4.0
        }, {
            name: 'Brio Klub',
            locationType: 'bar',
            loc: {
                lat: 49.212760,
                lon: 16.600726
            },
            rating: 3.6
        }, {
            name: 'Pohádková hospůdka',
            locationType: 'bar',
            loc: {
                lat: 49.218381,
                lon: 16.588045
            },
            rating: 4.0
        }, {
            name: 'Hospůdka u Jaróša',
            locationType: 'bar',
            loc: {
                lat: 49.221745,
                lon: 16.585406
            },
            rating: 3.6
        }, {
            name: 'BILLA',
            locationType: 'shop',
            loc: {
                lat: 49.210242,
                lon: 16.593496
            },
            rating: 3.1
        }, {
            name: 'Kaufland',
            locationType: 'shop',
            loc: {
                lat: 49.215359,
                lon: 16.604200
            },
            rating: 3.2
        }, {
            name: 'Nákupní centrum Královo Pole',
            locationType: 'shop',
            loc: {
                lat: 49.217636,
                lon: 16.607622
            },
            rating: 4.0
        }, {
            name: 'Supermarket Albert',
            locationType: 'shop',
            loc: {
                lat: 49.210218,
                lon: 16.600962
            },
            rating: 2.5
        }, {
            name: 'Potraviny Holánek',
            locationType: 'shop',
            loc: {
                lat: 49.210218,
                lon: 16.600962
            },
            rating: 2.1
        }, {
            name: 'Fotbal-shop',
            locationType: 'shop',
            loc: {
                lat: 49.192409,
                lon: 16.607029
            },
            rating: 3.5
        }, {
            name: 'Erotic City',
            locationType: 'shop',
            loc: {
                lat: 49.194414,
                lon: 16.609797
            },
            rating: 4.9
        }, {
            name: 'TESCO, obchodní dům',
            locationType: 'shop',
            loc: {
                lat: 49.189499,
                lon: 16.612984
            },
            rating: 3.6
        }, {
            name: 'Interspar - Brno - Vaňkovka',
            locationType: 'shop',
            loc: {
                lat: 49.188384,
                lon: 16.614872
            },
            rating: 3.1
        }


        , function () {
            console.log('finished populating locations');
        }
    );
});
