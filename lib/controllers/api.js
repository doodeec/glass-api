'use strict';

var mongoose = require('mongoose'),
    Location = mongoose.model('Location');

/**
 * Get all locations
 */
exports.allLocations = function (req, res) {
    return Location.find(function (err, things) {
        if (!err) {
            return res.json({places:things});
        } else {
            return res.send(err);
        }
    });
};

/**
 * Get all locations of given type
 */
exports.allLocationsOfType = function (req, res) {
    // ~2km radius
    return Location.find({locationType: req.query.type, loc: {$geoWithin: {$center: [[49.211750, 16.598490],0.02]}}}, function (err, things) {
        if (!err) {
            return res.json({places: things});
        } else {
            return res.send(err);
        }
    });
};

/**
 * Get all locations of given minimum rating
 */
exports.minRatingLocations = function (req, res) {
    return Location.find({rating: { $gt: req.query.minrating}}, function (err, things) {
        if (!err) {
            return res.json({places:things});
        } else {
            return res.send(err);
        }
    });
};

//TODO check if this is accurate
function getDistance(lat1, lon1, lat2, lon2) {  // generally used geo measurement function
    var R = 6378.137, // Radius of earth in KM
        dLat, dLon, a, c, d;
    dLat = (lat2 - lat1) * Math.PI / 180;
    dLon = (lon2 - lon1) * Math.PI / 180;
    a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    d = R * c;
    //in meters
    return d * 1000;
}
//FI muni location
//49.211750, 16.598490